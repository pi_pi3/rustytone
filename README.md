# rustytone

A framework for (live) coding music.  Inspired by Overtone, yet different.

## Examples

The example `sine.rs` can be run with `cargo run --example sine`.  The other
examples, `test.lpi` and `chiptune.lpi` need to be run through a client.  First
start a server with `cargo run`.  Then, while the server is running, run `cargo
run --example client < examples/chiptune.lpi` to execute the chiptune example.
Once you stop the client with CTRL-C, the server will start playing music.
