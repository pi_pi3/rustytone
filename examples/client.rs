use std::io::{self, Write};
use std::net::TcpStream;

const PORT: u16 = 35423;

fn main() -> Result<(), io::Error> {
    let mut stream = TcpStream::connect(("127.0.0.1", PORT))?;
    loop {
        let mut line = String::new();
        io::stdin().read_line(&mut line)?;
        stream.write_all(line.as_bytes())?;
    }
}
