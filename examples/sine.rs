use std::sync::Arc;

use rustytone::{Context, Metro, instr::Instr};

fn main() {
    let ctx = Context::new(48000);
    {
        let mut ctx_mut = ctx.borrow_mut();
        ctx_mut.set_metro(From::from("def".to_string()), Metro::new(2.0));
        let instr = Instr::Sine.freq(263.63).amp(0.5).dur(1.0);
        ctx_mut.add_instr(
            &From::from("def".to_string()),
            From::from("sine".to_string()),
            0.0,
            1.0,
            instr,
        );
    }
    Context::run(Arc::clone(&ctx));
    loop {}
}
