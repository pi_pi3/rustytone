use std::any::Any;
use std::convert::TryFrom;
use std::time::Duration;
use std::i32;

use hound::WavReader;
use hound::SampleFormat;

use lispi::error::{Error as LispiError, ErrorKind as LispiErrorKind};
use lispi::collections::{LinkedList, RcStr, Boxed, Mut};
use lispi::expr::{Value, ValueKind, Lambda, Ffi};

use crate::{Context, Metro};
use crate::error::Error;
use crate::instr::{Envelope, Instr};

#[derive(Debug, Clone)]
pub struct DefineComposition {
    ctx: Boxed<Mut<Context>>,
}

impl DefineComposition {
    pub fn new(ctx: Boxed<Mut<Context>>) -> Self {
        DefineComposition { ctx }
    }
}

impl Ffi for DefineComposition {
    fn display_name(&self) -> &str {
        "define-comp"
    }

    fn argc(&self) -> usize {
        3
    }

    fn call(&self, args: &[Value]) -> Result<Value, LispiError> {
        let cycle = Duration::from_float_secs(f64::try_from(args[0].clone())?);
        let dur = Duration::from_float_secs(f64::try_from(args[1].clone())?);
        let lambda = Boxed::<Lambda>::try_from(args[2].clone())?;
        self.ctx.borrow_mut().set_comp(cycle, dur, lambda);
        Ok(Value::from(LinkedList::new()))
    }
}

#[derive(Debug, Clone)]
pub struct SetMetro {
    ctx: Boxed<Mut<Context>>,
}

impl SetMetro {
    pub fn new(ctx: Boxed<Mut<Context>>) -> Self {
        SetMetro { ctx }
    }
}

impl Ffi for SetMetro {
    fn display_name(&self) -> &str {
        "set-metro"
    }

    fn argc(&self) -> usize {
        2
    }

    fn call(&self, args: &[Value]) -> Result<Value, LispiError> {
        let name = RcStr::try_from(args[0].clone())?;
        let metro = Boxed::<dyn Any + Send + Sync>::try_from(args[1].clone())?;
        let metro = metro
            .downcast_ref::<Metro>()
            .ok_or_else(|| LispiError::new(LispiErrorKind::InvalidType).describe("expected metro"))?
            .clone();
        self.ctx.borrow_mut().set_metro(name, metro);
        Ok(Value::from(LinkedList::new()))
    }
}

#[derive(Debug, Clone)]
pub struct AddInstr {
    ctx: Boxed<Mut<Context>>,
}

impl AddInstr {
    pub fn new(ctx: Boxed<Mut<Context>>) -> Self {
        AddInstr { ctx }
    }
}

impl Ffi for AddInstr {
    fn display_name(&self) -> &str {
        "add-instr"
    }

    fn argc(&self) -> usize {
        5
    }

    fn call(&self, args: &[Value]) -> Result<Value, LispiError> {
        let metro = RcStr::try_from(args[0].clone())?;
        let name = RcStr::try_from(args[1].clone())?;
        let offset = f64::try_from(args[2].clone())? as f32;
        let cycle = f64::try_from(args[3].clone())? as f32;
        let instr = Boxed::<dyn Any + Send + Sync>::try_from(args[4].clone())?;
        let instr = instr
            .downcast_ref::<Instr>()
            .ok_or_else(|| LispiError::new(LispiErrorKind::InvalidType).describe("expected instr"))?
            .clone();
        self.ctx
            .borrow_mut()
            .add_instr(&metro, name, offset, cycle, instr)
            .map_err(|err| LispiError::new(LispiErrorKind::User).with_cause(err))?;
        Ok(Value::from(LinkedList::new()))
    }
}

#[derive(Debug, Clone)]
pub struct DelInstr {
    ctx: Boxed<Mut<Context>>,
}

impl DelInstr {
    pub fn new(ctx: Boxed<Mut<Context>>) -> Self {
        DelInstr { ctx }
    }
}

impl Ffi for DelInstr {
    fn display_name(&self) -> &str {
        "del-instr"
    }

    fn argc(&self) -> usize {
        2
    }

    fn call(&self, args: &[Value]) -> Result<Value, LispiError> {
        let metro = RcStr::try_from(args[0].clone())?;
        let name = RcStr::try_from(args[1].clone())?;
        self.ctx
            .borrow_mut()
            .del_instr(&metro, &name)
            .map_err(|err| LispiError::new(LispiErrorKind::User).with_cause(err))?;
        Ok(Value::from(LinkedList::new()))
    }
}

#[derive(Debug, Clone)]
pub struct Metronome;

impl Ffi for Metronome {
    fn display_name(&self) -> &str {
        "metronome"
    }

    fn argc(&self) -> usize {
        1
    }

    fn call(&self, args: &[Value]) -> Result<Value, LispiError> {
        let cycle = f64::try_from(args[0].clone())? as f32;
        Ok(Value::from(
            Boxed::new(Metro::new(cycle)) as Boxed<dyn Any + Send + Sync>
        ))
    }
}

#[derive(Debug, Clone)]
pub struct Instrument;

impl Ffi for Instrument {
    fn display_name(&self) -> &str {
        "instr"
    }

    fn argc(&self) -> usize {
        1
    }

    fn call(&self, args: &[Value]) -> Result<Value, LispiError> {
        let list = LinkedList::try_from(args[0].clone())?;
        let instr = list.front().ok_or_else(|| {
            LispiError::new(LispiErrorKind::User)
                .span(args[0].span())
                .with_cause(Error::BadArgument("at least one argument in list"))
        })?;
        let mut instr = value_to_instr(instr)?;
        let iter = list
            .iter()
            .skip(1)
            .zip(list.iter().skip(2))
            .enumerate()
            .filter_map(|(i, val)| if i % 2 == 0 { Some(val) } else { None });
        for (key, value) in iter {
            instr = value_to_value(instr, key, value)?;
        }
        Ok(Value::from(Boxed::new(instr) as Boxed<dyn Any + Send + Sync>))
    }
}

#[derive(Debug, Clone)]
pub struct LoadSample;

impl Ffi for LoadSample {
    fn display_name(&self) -> &str {
        "load-sample"
    }

    fn argc(&self) -> usize {
        1
    }

    fn call(&self, args: &[Value]) -> Result<Value, LispiError> {
        let path = RcStr::try_from(args[0].clone())?;
        let reader =
            WavReader::open(path.as_str()).map_err(|err| LispiError::new(LispiErrorKind::User).with_cause(err))?;
        let frate = reader.spec().sample_rate as f32;
        let duration = reader.duration() as f32 / frate;
        let buffer = match reader.spec().sample_format {
            SampleFormat::Float => {
                reader
                    .into_samples::<f32>()
                    .fold(Ok(vec![]), |acc: Result<_, hound::Error>, x| {
                        let mut acc = acc?;
                        acc.push(x?);
                        Ok(acc)
                    })
                    .map_err(|err| LispiError::new(LispiErrorKind::User).with_cause(err))?
            }
            SampleFormat::Int => {
                let max = (1_u64 << (reader.spec().bits_per_sample - 1)) - 1;
                let max = max as f32;
                reader
                    .into_samples::<i32>()
                    .map(|x| x.map(|x| x as f32 / max))
                    .fold(Ok(vec![]), |acc: Result<_, hound::Error>, x| {
                        let mut acc = acc?;
                        acc.push(x?);
                        Ok(acc)
                    })
                    .map_err(|err| LispiError::new(LispiErrorKind::User).with_cause(err))?
            }
        };
        Ok(Value::from(
            Boxed::new(Instr::Buffer(buffer).freq(duration.recip())) as Boxed<dyn Any + Send + Sync>
        ))
    }
}

#[derive(Debug, Clone)]
pub struct LoadFile {
    ctx: Boxed<Mut<Context>>,
}

impl LoadFile {
    pub fn new(ctx: Boxed<Mut<Context>>) -> Self {
        LoadFile { ctx }
    }
}

impl Ffi for LoadFile {
    fn display_name(&self) -> &str {
        "load-file"
    }

    fn argc(&self) -> usize {
        1
    }

    fn call(&self, args: &[Value]) -> Result<Value, LispiError> {
        let path = RcStr::try_from(args[0].clone())?;
        let mut ctx_mut = self.ctx.borrow_mut();
        ctx_mut.load(path);
        Ok(Value::from(LinkedList::new()))
    }
}

#[derive(Debug, Clone)]
pub struct Play {
    ctx: Boxed<Mut<Context>>,
}

impl Play {
    pub fn new(ctx: Boxed<Mut<Context>>) -> Self {
        Play { ctx }
    }
}

impl Ffi for Play {
    fn display_name(&self) -> &str {
        "play"
    }

    fn argc(&self) -> usize {
        2
    }

    fn call(&self, args: &[Value]) -> Result<Value, LispiError> {
        let metro = RcStr::try_from(args[0].clone())?;
        let instr = RcStr::try_from(args[1].clone())?;
        self.ctx
            .borrow_mut()
            .play(&metro, &instr)
            .map_err(|err| LispiError::new(LispiErrorKind::User).with_cause(err))?;
        Ok(Value::from(LinkedList::new()))
    }
}

#[derive(Debug, Clone)]
pub struct Stop {
    ctx: Boxed<Mut<Context>>,
}

impl Stop {
    pub fn new(ctx: Boxed<Mut<Context>>) -> Self {
        Stop { ctx }
    }
}

impl Ffi for Stop {
    fn display_name(&self) -> &str {
        "stop"
    }

    fn argc(&self) -> usize {
        0
    }

    fn call(&self, _: &[Value]) -> Result<Value, LispiError> {
        let mut ctx_mut = self.ctx.borrow_mut();
        ctx_mut.stop = true;
        Ok(Value::from("stopping server NOW!".to_string()))
    }
}

fn value_to_instr(value: &Value) -> Result<Instr, LispiError> {
    match value.kind() {
        ValueKind::Atom(name) if name.as_str() == "sin" => Ok(Instr::Sine),
        ValueKind::Atom(name) if name.as_str() == "sawtooth" => Ok(Instr::Sawtooth),
        ValueKind::Atom(name) if name.as_str() == "square" => Ok(Instr::Square),
        ValueKind::Atom(name) if name.as_str() == "triangle" => Ok(Instr::Triangle),
        ValueKind::Atom(name) if name.as_str() == "noise" => Ok(Instr::Noise),
        ValueKind::Any(instr) => {
            let instr: Option<&Instr> = instr.downcast_ref();
            if let Some(instr) = instr {
                Ok(instr.clone())
            } else {
                Err(LispiError::new(LispiErrorKind::InvalidType).describe("expected instr"))
            }
        }
        ValueKind::List(list) => {
            let key = list.list().front().ok_or_else(|| {
                LispiError::new(LispiErrorKind::User)
                    .span(list.span())
                    .with_cause(Error::BadArgument("at least one keyword in list"))
            })?;
            match key.kind() {
                ValueKind::Atom(name) if name.as_str() == "+" => {
                    let sum = list
                        .list()
                        .iter()
                        .skip(1)
                        .map(value_to_instr)
                        .fold(Ok(vec![]), |acc, instr| {
                            let mut acc = acc?;
                            acc.push(instr?);
                            Ok(acc)
                        });
                    Ok(Instr::Sum(Boxed::new(sum?)))
                }
                ValueKind::Atom(name) if name.as_str() == "*" => {
                    let sum = list
                        .list()
                        .iter()
                        .skip(1)
                        .map(value_to_instr)
                        .fold(Ok(vec![]), |acc, instr| {
                            let mut acc = acc?;
                            acc.push(instr?);
                            Ok(acc)
                        });
                    Ok(Instr::Chain(Boxed::new(sum?)))
                }
                ValueKind::Atom(name) if name.as_str() == "tremolo" => {
                    let instr = value_to_instr(&list.list().nth(1).ok_or_else(|| {
                        LispiError::new(LispiErrorKind::User)
                            .span(list.span())
                            .with_cause(Error::BadArgument("2 arguments in `tremolo`"))
                    })?)?;
                    let freq = f64::try_from(list.list().nth(2).ok_or_else(|| {
                        LispiError::new(LispiErrorKind::User)
                            .span(list.span())
                            .with_cause(Error::BadArgument("2 arguments in `tremolo`"))
                    })?)? as f32;
                    Ok(Instr::Tremolo(Boxed::new(instr), freq))
                }
                ValueKind::Atom(name) if name.as_str() == "vibrato" => {
                    let instr = value_to_instr(&list.list().nth(1).ok_or_else(|| {
                        LispiError::new(LispiErrorKind::User)
                            .span(list.span())
                            .with_cause(Error::BadArgument("2 arguments in `vibrato`"))
                    })?)?;
                    let freq = f64::try_from(list.list().nth(2).ok_or_else(|| {
                        LispiError::new(LispiErrorKind::User)
                            .span(list.span())
                            .with_cause(Error::BadArgument("2 arguments in `vibrato`"))
                    })?)? as f32;
                    Ok(Instr::Vibrato(Boxed::new(instr), freq))
                }
                ValueKind::Atom(_) => {
                    Err(LispiError::new(LispiErrorKind::User)
                        .span(key.span())
                        .with_cause(Error::BadArgument("+, *, tremolo or vibrato")))
                }
                _ => Err(LispiError::new(LispiErrorKind::InvalidType).describe("expected atom")),
            }
        }
        _ => {
            Err(LispiError::new(LispiErrorKind::InvalidType)
                .describe(format!("expected atom, instrument or list, got {}", value.type_of())))
        }
    }
}

fn value_to_value(instr: Instr, key: &Value, value: &Value) -> Result<Instr, LispiError> {
    match value.kind() {
        ValueKind::Float(x) => {
            let x = *x as f32;
            match key.kind() {
                ValueKind::Atom(key) if key.as_str() == "amp" => Ok(instr.amp(x)),
                ValueKind::Atom(key) if key.as_str() == "freq" => Ok(instr.freq(x)),
                ValueKind::Atom(key) if key.as_str() == "dur" => Ok(instr.dur(x)),
                ValueKind::Atom(_) => {
                    Err(LispiError::new(LispiErrorKind::User)
                        .span(key.span())
                        .with_cause(Error::BadArgument("amp, freq or dur")))
                }
                _ => Err(LispiError::new(LispiErrorKind::InvalidType).describe("expected atom")),
            }
        }
        ValueKind::List(list) => {
            let env = list.list().front().ok_or_else(|| {
                LispiError::new(LispiErrorKind::User)
                    .span(list.span())
                    .with_cause(Error::BadArgument("at least one keyword in list"))
            })?;
            let env = match env.kind() {
                ValueKind::Atom(name) if name.as_str() == "const" => {
                    let a = f64::try_from(list.list().nth(1).ok_or_else(|| {
                        LispiError::new(LispiErrorKind::User)
                            .span(list.span())
                            .with_cause(Error::BadArgument("1 argument in `const`"))
                    })?)?;
                    Envelope::Const(a as f32)
                }
                ValueKind::Atom(name) if name.as_str() == "lin" => {
                    let a = f64::try_from(list.list().nth(1).ok_or_else(|| {
                        LispiError::new(LispiErrorKind::User)
                            .span(list.span())
                            .with_cause(Error::BadArgument("2 arguments in `lin`"))
                    })?)?;
                    let b = f64::try_from(list.list().nth(2).ok_or_else(|| {
                        LispiError::new(LispiErrorKind::User)
                            .span(list.span())
                            .with_cause(Error::BadArgument("2 arguments in `lin`"))
                    })?)?;
                    Envelope::Lin(a as f32, b as f32)
                }
                ValueKind::Atom(name) if name.as_str() == "sqr" => {
                    let a = f64::try_from(list.list().nth(1).ok_or_else(|| {
                        LispiError::new(LispiErrorKind::User)
                            .span(list.span())
                            .with_cause(Error::BadArgument("3 arguments in `sqr`"))
                    })?)?;
                    let b = f64::try_from(list.list().nth(2).ok_or_else(|| {
                        LispiError::new(LispiErrorKind::User)
                            .span(list.span())
                            .with_cause(Error::BadArgument("3 arguments in `sqr`"))
                    })?)?;
                    let c = f64::try_from(list.list().nth(3).ok_or_else(|| {
                        LispiError::new(LispiErrorKind::User)
                            .span(list.span())
                            .with_cause(Error::BadArgument("3 arguments in `sqr`"))
                    })?)?;
                    Envelope::Sqr(a as f32, b as f32, c as f32)
                }
                ValueKind::Atom(name) if name.as_str() == "adsr" => {
                    let a = f64::try_from(list.list().nth(1).ok_or_else(|| {
                        LispiError::new(LispiErrorKind::User)
                            .span(list.span())
                            .with_cause(Error::BadArgument("4 arguments in `adsr`"))
                    })?)?;
                    let d = f64::try_from(list.list().nth(2).ok_or_else(|| {
                        LispiError::new(LispiErrorKind::User)
                            .span(list.span())
                            .with_cause(Error::BadArgument("4 arguments in `adsr`"))
                    })?)?;
                    let s = f64::try_from(list.list().nth(3).ok_or_else(|| {
                        LispiError::new(LispiErrorKind::User)
                            .span(list.span())
                            .with_cause(Error::BadArgument("4 arguments in `adsr`"))
                    })?)?;
                    let r = f64::try_from(list.list().nth(4).ok_or_else(|| {
                        LispiError::new(LispiErrorKind::User)
                            .span(list.span())
                            .with_cause(Error::BadArgument("4 arguments in `adsr`"))
                    })?)?;
                    Envelope::Adsr(a as f32, d as f32, s as f32, r as f32)
                }
                ValueKind::Atom(_) => {
                    return Err(LispiError::new(LispiErrorKind::User)
                        .span(key.span())
                        .with_cause(Error::BadArgument("const, lin, sqr or adsr")));
                }
                _ => return Err(LispiError::new(LispiErrorKind::InvalidType).describe("expected atom")),
            };
            match key.kind() {
                ValueKind::Atom(key) if key.as_str() == "amp" => Ok(instr.env_amp(env)),
                ValueKind::Atom(key) if key.as_str() == "freq" => Ok(instr.env_freq(env)),
                ValueKind::Atom(_) => {
                    Err(LispiError::new(LispiErrorKind::User)
                        .span(key.span())
                        .with_cause(Error::BadArgument("amp, freq or dur")))
                }
                _ => Err(LispiError::new(LispiErrorKind::InvalidType).describe("expected atom")),
            }
        }
        _ => {
            Err(LispiError::new(LispiErrorKind::InvalidType)
                .describe(format!("expected list or atom, got {}", value.type_of())))
        }
    }
}
