use std::ops::{Add, Mul};
use std::f32::consts::PI;

use lispi::collections::Boxed;

const SUSTAIN_LEVEL: f32 = 0.5;

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum Envelope {
    Const(f32),
    Lin(f32, f32),
    Sqr(f32, f32, f32),
    Adsr(f32, f32, f32, f32),
}

impl Envelope {
    pub fn env(&self, t: f32) -> f32 {
        match self {
            Envelope::Const(a) => *a,
            Envelope::Lin(a, b) => a * t + b,
            Envelope::Sqr(a, b, c) => a * t * t + b * t + c,
            Envelope::Adsr(a, d, s, r) => {
                if t < 0.0 {
                    0.0
                } else if t < *a {
                    a.recip() * t
                } else if t < *a + *d {
                    -0.5 * d.recip() * t
                } else if t < *a + *d + *s {
                    SUSTAIN_LEVEL
                } else if t < *a + *d + *s + *r {
                    -0.5 * r.recip() * t
                } else {
                    0.0
                }
            }
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum Instr {
    Sine,
    Sawtooth,
    Square,
    Triangle,
    Noise,
    Buffer(Vec<f32>),
    Sum(Boxed<Vec<Instr>>),
    Chain(Boxed<Vec<Instr>>),
    Amp(Boxed<Instr>, f32),
    Freq(Boxed<Instr>, f32),
    Dur(Boxed<Instr>, f32),
    EnvAmp(Boxed<Instr>, Envelope),
    EnvFreq(Boxed<Instr>, Envelope),
    Tremolo(Boxed<Instr>, f32),
    Vibrato(Boxed<Instr>, f32),
}

impl Instr {
    pub fn amp(self, amp: f32) -> Instr {
        Instr::Amp(Boxed::new(self), amp)
    }

    pub fn freq(self, freq: f32) -> Instr {
        Instr::Freq(Boxed::new(self), freq)
    }

    pub fn env_amp(self, amp: Envelope) -> Instr {
        Instr::EnvAmp(Boxed::new(self), amp)
    }

    pub fn env_freq(self, freq: Envelope) -> Instr {
        Instr::EnvFreq(Boxed::new(self), freq)
    }

    pub fn tremolo(self, freq: f32) -> Instr {
        Instr::Tremolo(Boxed::new(self), freq)
    }

    pub fn vibrato(self, freq: f32) -> Instr {
        Instr::Vibrato(Boxed::new(self), freq)
    }

    pub fn dur(self, dur: f32) -> Instr {
        Instr::Dur(Boxed::new(self), dur)
    }

    pub fn iter<'a, 'b: 'a>(&'a self, irate: u64, duration: f32) -> impl Iterator<Item = f32> + 'a {
        let frate = irate as f32;
        let duration = (irate as f32 * duration) as u64;
        (0..duration)
            .map(move |i| i as f32 / frate)
            .filter_map(move |t| self.sample(t))
    }

    pub fn sample(&self, t: f32) -> Option<f32> {
        match self {
            Instr::Sine => Some((t * 2. * PI).sin()),
            Instr::Sawtooth => Some(t % 2. - 1.),
            Instr::Square => {
                if t % 2. - 1. >= 0. {
                    Some(1.0)
                } else {
                    Some(-1.0)
                }
            }
            Instr::Triangle => {
                if (2. * t) as i64 % 2 == 0 {
                    Some(2. * ((4. * t) % 2. - 1.))
                } else {
                    Some(-2. * ((4. * t) % 2. - 1.))
                }
            }
            Instr::Noise => Some(rand::random::<f32>()),
            Instr::Buffer(buf) => {
                if t >= 1. || t < 0. {
                    None
                } else {
                    Some(buf[(buf.len() as f32 * t) as usize])
                }
            }
            Instr::Sum(sum) => {
                sum.iter().map(|a| a.sample(t)).fold(Some(0.0), |acc, sample| {
                    let sample = sample?;
                    acc.map(|acc| acc + sample)
                })
            }
            Instr::Chain(chain) => {
                chain
                    .iter()
                    .fold(None, |acc, instr| if acc.is_some() { acc } else { instr.sample(t) })
            }
            Instr::Amp(sound, amp) => sound.sample(t).map(|x| x * amp.max(0.)),
            Instr::Freq(sound, freq) => sound.sample(t * freq),
            Instr::Dur(sound, dur) => {
                if t <= *dur {
                    sound.sample(t)
                } else {
                    None
                }
            }
            Instr::EnvAmp(sound, amp) => sound.sample(t).map(|x| x * amp.env(t).max(0.)),
            Instr::EnvFreq(sound, freq) => sound.sample(t * freq.env(t)),
            Instr::Tremolo(sound, freq) => sound.sample(t).map(|x| x * ((freq * t).sin() * 0.5 + 0.5)),
            Instr::Vibrato(sound, freq) => sound.sample(t * ((freq * t).sin() * 0.5 + 0.5)),
        }
    }
}

impl Add for Instr {
    type Output = Instr;

    fn add(self, other: Self) -> Self {
        Instr::Sum(Boxed::new(vec![self, other]))
    }
}

impl Mul for Instr {
    type Output = Instr;

    fn mul(self, other: Self) -> Self {
        Instr::Chain(Boxed::new(vec![self, other]))
    }
}
