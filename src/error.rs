use std::io;

use lispi::error::Error as LispiError;

#[derive(Debug, Fail)]
pub enum Error {
    #[fail(display = "io error: {}", _0)]
    Io(io::Error),
    #[fail(display = "couldn't join thread")]
    ThreadPanicked,
    #[fail(display = "lispi error: {}", _0)]
    LispiError(LispiError),
    #[fail(display = "metronome: {:?} was not found", _0)]
    MetroNotFound(String),
    #[fail(display = "instrument: {:?} was not found", _0)]
    InstrNotFound(String),
    #[fail(display = "this context is already running")]
    ContextAlreadyRunning,
    #[fail(display = "this context is not running yet")]
    ContextNotRunning,
    #[fail(display = "no default output device was found")]
    NoOutputDevice,
    #[fail(display = "bad argument in expression, expected: {}", _0)]
    BadArgument(&'static str),
}

impl From<LispiError> for Error {
    fn from(err: LispiError) -> Self {
        Error::LispiError(err)
    }
}

impl From<io::Error> for Error {
    fn from(err: io::Error) -> Self {
        Error::Io(err)
    }
}
