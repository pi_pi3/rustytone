#![feature(duration_float)]
#![feature(try_from)]

#[macro_use]
extern crate failure;

use std::fs;
use std::mem;
use std::time::{Instant, Duration};
use std::collections::{HashMap, VecDeque};
use std::thread::{self, JoinHandle};
use std::convert::TryFrom;

use lispi::Lispi;
use lispi::collections::{LinkedList, RcStr, Boxed, Mut};
use lispi::expr::{Value, Lambda};

use self::error::Error;
use self::instr::Instr;

pub mod error;
pub mod instr;
mod ffi;

static PRELUDE: &str = include_str!("prelude.lpi");

#[derive(Debug)]
#[must_use]
pub struct RunHandle {
    ctx: Boxed<Mut<Context>>,
}

impl RunHandle {
    pub fn is_stop(&self) -> bool {
        self.ctx.borrow().stop
    }

    pub fn stop(self) -> Result<(), Error> {
        let ctx = Boxed::clone(&self.ctx);
        mem::drop(self);
        thread::sleep(Duration::from_millis(10));
        ctx.borrow_mut()
            .rt
            .take()
            .unwrap()
            .join()
            .map_err(|_| Error::ThreadPanicked)??;
        Ok(())
    }
}

impl Drop for RunHandle {
    fn drop(&mut self) {
        self.ctx.borrow_mut().stop = true;
    }
}

#[derive(Debug)]
pub struct Context {
    interp: Option<Lispi>,
    composition: Option<Composition>,
    metros: Option<HashMap<RcStr, Metro>>,
    rt: Option<JoinHandle<Result<(), Error>>>,
    load: Option<VecDeque<RcStr>>,
    irate: u64,
    stop: bool,
}

impl Context {
    pub fn new(irate: u64) -> Result<Boxed<Mut<Context>>, Error> {
        let mut interp = Lispi::default();
        interp.load_std()?;
        interp.eval(Some("prelude.lpi"), PRELUDE)?;
        let ctx = Context {
            interp: Some(interp),
            composition: None,
            metros: Some(HashMap::new()),
            rt: None,
            load: Some(VecDeque::new()),
            irate,
            stop: false,
        };
        let ctx = Boxed::new(Mut::new(ctx));
        {
            let mut ctx_mut = ctx.borrow_mut();
            ctx_mut
                .interp_mut()
                .register_ffi(ffi::DefineComposition::new(Boxed::clone(&ctx)))?;
            ctx_mut
                .interp_mut()
                .register_ffi(ffi::SetMetro::new(Boxed::clone(&ctx)))?;
            ctx_mut
                .interp_mut()
                .register_ffi(ffi::AddInstr::new(Boxed::clone(&ctx)))?;
            ctx_mut
                .interp_mut()
                .register_ffi(ffi::DelInstr::new(Boxed::clone(&ctx)))?;
            ctx_mut.interp_mut().register_ffi(ffi::Metronome)?;
            ctx_mut.interp_mut().register_ffi(ffi::Instrument)?;
            ctx_mut.interp_mut().register_ffi(ffi::LoadSample)?;
            ctx_mut
                .interp_mut()
                .register_ffi(ffi::LoadFile::new(Boxed::clone(&ctx)))?;
            ctx_mut.interp_mut().register_ffi(ffi::Play::new(Boxed::clone(&ctx)))?;
            ctx_mut.interp_mut().register_ffi(ffi::Stop::new(Boxed::clone(&ctx)))?;
        }
        Ok(ctx)
    }

    pub fn run(this: Boxed<Mut<Context>>) -> Result<RunHandle, Error> {
        let mut this_mut = this.borrow_mut();
        if this_mut.rt.is_some() {
            return Err(Error::ContextAlreadyRunning);
        }

        let this_clone = Boxed::clone(&this);
        this_mut.stop = false;
        this_mut.rt = Some(thread::spawn(move || {
            let this = this_clone;
            loop {
                let mut this_mut = this.borrow_mut();
                if this_mut.stop {
                    break;
                }

                if let Some(mut composition) = this_mut.composition.take() {
                    let time = Instant::now();
                    if time >= composition.start + composition.cycle {
                        composition.start += composition.cycle;
                        composition.next = Instant::now();
                    }
                    if time >= composition.next {
                        let t = time.duration_since(composition.start).as_float_secs();
                        composition.next += composition.dur;
                        if let Some(mut interp) = this_mut.interp.take() {
                            let list = interp.call_anonymous(Lambda::clone(&composition.lambda), &[Value::from(t)])?;
                            let list = LinkedList::try_from(list)?;

                            let mut metros = this_mut.metros.take().unwrap();
                            for metro in metros.values_mut() {
                                metro.active = false;
                            }

                            for metro in &list {
                                let metro = RcStr::try_from(metro.clone())?;
                                metros
                                    .get_mut(&metro)
                                    .ok_or_else(|| Error::MetroNotFound(metro.to_string()))?
                                    .active = true;
                            }

                            this_mut.metros = Some(metros);
                            this_mut.set_interp(interp);
                        }
                    }
                    this_mut.composition = Some(composition);
                }

                let mut metros = this_mut.metros.take().unwrap();
                let irate = this_mut.irate;
                for metro in metros.values_mut() {
                    let t1 = Instant::now();
                    let idelta = t1.duration_since(metro.t0);
                    if metro.active && idelta < metro.cycle {
                        let fdelta = idelta.as_float_secs();
                        for instance in metro.instances.values_mut() {
                            let offset = instance.offset.as_float_secs() * metro.cycle.as_float_secs();
                            let cycle =
                                instance.cycle.as_float_secs() * metro.cycle.as_float_secs() * instance.i as f64;
                            if fdelta >= offset {
                                if fdelta >= cycle + offset {
                                    instance.i += 1;
                                    let buffer = instance
                                        .instr
                                        .iter(irate, metro.cycle.as_float_secs() as f32)
                                        .collect::<Vec<_>>();
                                    let buffer = rodio::buffer::SamplesBuffer::new(1, irate as _, buffer);
                                    let sink = rodio::Sink::new(
                                        &rodio::default_output_device().ok_or_else(|| Error::NoOutputDevice)?,
                                    );
                                    sink.append(buffer);
                                    sink.play();
                                    sink.detach();
                                }
                            }
                        }
                    } else {
                        for instance in metro.instances.values_mut() {
                            instance.i = 0;
                        }
                        metro.t0 = t1;
                    }
                }
                this_mut.metros = Some(metros);
                mem::drop(this_mut);
                thread::sleep(Duration::from_millis(1));
            }
            Ok(())
        }));
        mem::drop(this_mut);
        Ok(RunHandle { ctx: this })
    }

    pub fn play(&self, metro: &RcStr, instr: &RcStr) -> Result<(), Error> {
        let irate = self.irate;
        let metro = self
            .metros
            .as_ref()
            .unwrap()
            .get(metro)
            .ok_or_else(|| Error::MetroNotFound(metro.to_string()))?;
        let instance = metro
            .instances
            .get(instr)
            .ok_or_else(|| Error::InstrNotFound(instr.to_string()))?;
        let buffer = instance
            .instr
            .iter(irate, metro.cycle.as_float_secs() as f32)
            .collect::<Vec<_>>();
        let buffer = rodio::buffer::SamplesBuffer::new(1, irate as _, buffer);
        let sink = rodio::Sink::new(&rodio::default_output_device().ok_or_else(|| Error::NoOutputDevice)?);
        sink.append(buffer);
        sink.play();
        sink.detach();
        Ok(())
    }

    pub fn load<S: Into<RcStr>>(&mut self, file: S) {
        self.load.as_mut().unwrap().push_back(file.into());
    }

    pub fn do_load(this: Boxed<Mut<Context>>) -> Result<Option<Value>, Error> {
        let mut last = None;
        let (mut interp, queue) = {
            let mut this_mut = this.borrow_mut();
            let queue = this_mut.load.take().unwrap();
            this_mut.load = Some(VecDeque::new());
            (this_mut.interp(), queue)
        };
        for path in queue {
            let file = fs::read_to_string(path.as_str())?;
            last = interp.eval(Some(&path), &file)?;
        }
        let mut this_mut = this.borrow_mut();
        this_mut.set_interp(interp);
        Ok(last)
    }

    pub fn interp(&mut self) -> Lispi {
        self.interp.take().unwrap()
    }

    pub fn set_interp(&mut self, interp: Lispi) {
        self.interp = Some(interp);
    }

    pub fn interp_mut(&mut self) -> &mut Lispi {
        self.interp.as_mut().unwrap()
    }

    pub fn set_metro(&mut self, name: RcStr, metro: Metro) {
        self.metros.as_mut().unwrap().insert(name, metro);
    }

    pub fn set_comp(&mut self, cycle: Duration, dur: Duration, lambda: Boxed<Lambda>) {
        for metro in self.metros.as_mut().unwrap().values_mut() {
            metro.active = false;
        }
        self.composition = Some(Composition {
            next: Instant::now(),
            start: Instant::now(),
            cycle,
            dur,
            lambda,
        });
    }

    pub fn add_instr(
        &mut self,
        metro: &RcStr,
        name: RcStr,
        offset: f32,
        cycle: f32,
        instr: Instr,
    ) -> Result<(), Error> {
        self.metros
            .as_mut()
            .unwrap()
            .get_mut(metro)
            .ok_or_else(|| Error::MetroNotFound(metro.to_string()))?
            .instances
            .insert(
                name,
                InstrInst {
                    offset: Duration::from_float_secs(offset as _),
                    cycle: Duration::from_float_secs(cycle as _),
                    instr,
                    i: 0,
                },
            );
        Ok(())
    }

    pub fn del_instr(&mut self, metro: &RcStr, name: &RcStr) -> Result<(), Error> {
        self.metros
            .as_mut()
            .unwrap()
            .get_mut(metro)
            .ok_or_else(|| Error::MetroNotFound(metro.to_string()))?
            .instances
            .remove(name);
        Ok(())
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct Metro {
    active: bool,
    t0: Instant,
    cycle: Duration,
    instances: HashMap<RcStr, InstrInst>,
}

impl Metro {
    pub fn new(cycle: f32) -> Self {
        Metro {
            active: true,
            t0: Instant::now(),
            cycle: Duration::from_float_secs(cycle as _),
            instances: HashMap::new(),
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct InstrInst {
    offset: Duration,
    cycle: Duration,
    instr: Instr,
    i: u32,
}

#[derive(Debug, Clone, PartialEq)]
pub struct Composition {
    next: Instant,
    start: Instant,
    dur: Duration,
    cycle: Duration,
    lambda: Boxed<Lambda>,
}
