#[macro_use]
extern crate failure;

use std::process;
use std::fs::{self, File};
use std::io::{self, Read, Write};
use std::path::Path;
use std::net::{TcpListener, TcpStream, Shutdown};
use std::sync::Arc;
use std::collections::{BTreeSet, BTreeMap};

use clap::{App, SubCommand, Arg};
use rustyline::Editor;
use rustyline::error::ReadlineError;
use winit::{EventsLoop, WindowBuilder, Event, WindowEvent, ControlFlow, dpi::LogicalSize, ScanCode, ElementState, CreationError};

use rustytone::Context;
use rustytone::error::Error;

pub const NAME: &str = env!("CARGO_PKG_NAME");
pub const VERSION: &str = env!("CARGO_PKG_VERSION");
pub const AUTHOR: &str = env!("CARGO_PKG_AUTHORS");
pub const ABOUT: &str = "rustytone front- and backend";
const ART: &str = "";
const PROMPT: &str = ">>> ";
const PROMPT2: &str = ">>>   ";
const RETURN: &str = "= ";
const HIST_FILE: &str = ".rustytone_history";
const KEYBOARD_CONFIG: &str = "keyboard.ron";

fn subcommand_server(port: u16) -> Result<(), Error> {
    let listener = TcpListener::bind(("127.0.0.1", port))?;

    let ctx = match Context::new(48000) {
        Ok(ctx) => ctx,
        Err(err) => {
            eprintln!("{}", err);
            process::exit(1);
        }
    };
    let run = Context::run(Arc::clone(&ctx)).unwrap_or_else(|err| panic!("{}", err));
    for stream in listener.incoming() {
        let mut stream = stream?;
        println!(": {}", stream.peer_addr()?);
        let mut prog = String::new();
        stream.read_to_string(&mut prog)?;
        let mut interp = {
            let mut ctx_mut = ctx.borrow_mut();
            ctx_mut.interp()
        };
        let response = match interp.eval(Some(&format!("<{}>", stream.peer_addr()?.to_string())), &prog) {
            Ok(Some(ok)) => format!("{}{}", RETURN, ok),
            Ok(None) => String::new(),
            Err(err) => {
                let err = err.to_string();
                eprintln!("{}", err);
                err
            }
        };
        stream.write_all(response.as_bytes())?;
        {
            let mut ctx_mut = ctx.borrow_mut();
            ctx_mut.set_interp(interp);
        }
        if let Err(err) = Context::do_load(Arc::clone(&ctx)) {
            eprintln!("{}", err);
        }
        if run.is_stop() {
            break;
        }
    }
    run.stop()?;
    Ok(())
}

fn subcommand_repl(addr: &str, port: u16) -> Result<(), ReadlineError> {
    println!("{}", ART);
    println!("{} {}", NAME, VERSION);
    let mut rl = Editor::<()>::new();
    let _ = rl.load_history(HIST_FILE);
    let mut last: Option<String> = None;
    loop {
        let prompt = if last.is_some() { PROMPT2 } else { PROMPT };
        let readline = rl.readline(prompt);
        match readline {
            Ok(line) => {
                let line = match last {
                    Some(mut last) => {
                        last.push_str("\n      ");
                        last.push_str(&line);
                        last
                    }
                    None => line,
                };
                let paren = {
                    let mut paren = 0;
                    let mut string = false;
                    let mut backslash = false;
                    for c in line.chars() {
                        match (string, backslash) {
                            (false, _) => {
                                match c {
                                    '(' => paren += 1,
                                    ')' => paren -= 1,
                                    '"' => backslash = true,
                                    _ => {}
                                }
                            }
                            (true, false) => {
                                match c {
                                    '"' => string = false,
                                    '\\' => backslash = true,
                                    _ => {}
                                }
                            }
                            (true, true) => backslash = false,
                        }
                    }
                    paren
                };

                if paren > 0 {
                    last = Some(line);
                    continue;
                }
                rl.add_history_entry(line.as_ref());
                let mut stream = TcpStream::connect((addr, port))?;
                stream.write_all(line.as_bytes())?;
                stream.shutdown(Shutdown::Write)?;
                let mut response = String::new();
                stream.read_to_string(&mut response)?;
                println!("{}", response);
                last = None;
            }
            Err(ReadlineError::Interrupted) => {
                println!("^C");
                break;
            }
            Err(ReadlineError::Eof) => {
                println!("^D");
                break;
            }
            Err(err) => {
                if let Err(err) = rl.save_history(HIST_FILE) {
                    eprintln!("could't write history file: {}", err);
                }
                return Err(err);
            }
        }
    }
    if let Err(err) = rl.save_history(HIST_FILE) {
        eprintln!("could't write history file: {}", err);
        process::exit(1);
    }
    Ok(())
}

fn subcommand_file<P: AsRef<Path>>(file: P, addr: &str, port: u16) -> Result<(), Error> {
    let mut stream = TcpStream::connect((addr, port))?;
    let file = fs::read_to_string(file)?;
    stream.write_all(file.as_bytes())?;
    stream.shutdown(Shutdown::Write)?;
    let mut response = String::new();
    stream.read_to_string(&mut response)?;
    println!("{}", response);
    Ok(())
}

#[derive(Debug, Fail)]
enum InteractiveError {
    #[fail(display = "couldn't create window: {}", _0)]
    Creation(CreationError),
    #[fail(display = "io error: {}", _0)]
    Io(io::Error),
    #[fail(display = "deserialization: {}", _0)]
    Deserialize(ron::de::Error),
}

impl From<CreationError> for InteractiveError {
    fn from(err: CreationError) -> InteractiveError {
        InteractiveError::Creation(err)
    }
}

impl From<io::Error> for InteractiveError {
    fn from(err: io::Error) -> InteractiveError {
        InteractiveError::Io(err)
    }
}

impl From<ron::de::Error> for InteractiveError {
    fn from(err: ron::de::Error) -> InteractiveError {
        InteractiveError::Deserialize(err)
    }
}

fn subcommand_interactive(addr: &str, port: u16) -> Result<(), InteractiveError> {
    let mut events_loop = EventsLoop::new();
    let _window = WindowBuilder::new()
        .with_title("rustytone interactive")
        .with_dimensions(LogicalSize::from_physical((360., 240.), 3.16666))
        .build(&events_loop)?;
    let mut keyset = BTreeSet::new();
    let config = File::open(KEYBOARD_CONFIG)?;
    let keys: BTreeMap<ScanCode, (String, String)> = ron::de::from_reader(config)?;
    let mut error = None;
    events_loop.run_forever(|event| {
        match event {
            Event::WindowEvent {
                event: WindowEvent::CloseRequested,
                ..
            } => ControlFlow::Break,
            Event::WindowEvent {
                event: WindowEvent::KeyboardInput { input, .. },
                ..
            } => {
                match input.state {
                    ElementState::Pressed => {
                        if !keyset.contains(&input.scancode) {
                            if let Some((metro, instr)) = keys.get(&input.scancode) {
                                let mut stream = match TcpStream::connect((addr, port)) {
                                    Ok(stream) => stream,
                                    Err(err) => {
                                        error = Some(From::from(err));
                                        return ControlFlow::Break;
                                    }
                                };
                                if let Err(err) = write!(stream, "(play {:?} {:?})", metro, instr) {
                                    error = Some(From::from(err));
                                    return ControlFlow::Break;
                                }
                                if let Err(err) = stream.shutdown(Shutdown::Write) {
                                    error = Some(From::from(err));
                                    return ControlFlow::Break;
                                }
                                let mut response = String::new();
                                if let Err(err) = stream.read_to_string(&mut response){
                                    error = Some(From::from(err));
                                    return ControlFlow::Break;
                                }
                                println!("{}", response);
                            }
                        }
                        keyset.insert(input.scancode);
                    }
                    ElementState::Released => {
                        keyset.remove(&input.scancode);
                    }
                }
                ControlFlow::Continue
            }
            _ => ControlFlow::Continue,
        }
    });
    if let Some(err) = error {
        Err(err)
    } else {
        Ok(())
    }
}

fn main() {
    let matches = App::new(NAME)
        .version(VERSION)
        .author(AUTHOR)
        .about(ABOUT)
        .subcommand(
            SubCommand::with_name("server").about("Runs the server").arg(
                Arg::with_name("port")
                    .short("p")
                    .long("port")
                    .takes_value(true)
                    .help("Use the following port in the server"),
            ),
        )
        .subcommand(
            SubCommand::with_name("repl")
                .about("Runs an interactive repl")
                .arg(
                    Arg::with_name("address")
                        .short("a")
                        .long("address")
                        .takes_value(true)
                        .help("Use the following address to connect to the server"),
                )
                .arg(
                    Arg::with_name("port")
                        .short("p")
                        .long("port")
                        .takes_value(true)
                        .help("Use the following port to connect to the server"),
                ),
        )
        .subcommand(
            SubCommand::with_name("file")
                .about("Executes a file")
                .arg(
                    Arg::with_name("address")
                        .short("a")
                        .long("address")
                        .takes_value(true)
                        .help("Use the following address to connect to the server"),
                )
                .arg(
                    Arg::with_name("port")
                        .short("p")
                        .long("port")
                        .takes_value(true)
                        .help("Use the following port to connect to the server"),
                )
                .arg(
                    Arg::with_name("file")
                        .takes_value(true)
                        .required(true)
                        .help("Loads the following file"),
                ),
        )
        .subcommand(
            SubCommand::with_name("keyboard")
                .about("Runs an interactive keyboard mode")
                .arg(
                    Arg::with_name("address")
                        .short("a")
                        .long("address")
                        .takes_value(true)
                        .help("Use the following address to connect to the server"),
                )
                .arg(
                    Arg::with_name("port")
                        .short("p")
                        .long("port")
                        .takes_value(true)
                        .help("Use the following port to connect to the server"),
                ),
        )
        .get_matches();

    if let Some(matches) = matches.subcommand_matches("server") {
        match matches.value_of("port").unwrap_or("35423").parse() {
            Ok(port) => {
                if let Err(err) = subcommand_server(port) {
                    eprintln!("{}", err);
                    process::exit(1);
                }
            }
            Err(err) => {
                eprintln!("{}", err);
                process::exit(1);
            }
        }
    }

    if let Some(matches) = matches.subcommand_matches("repl") {
        let address = matches.value_of("address").unwrap_or("127.0.0.1");
        let port = match matches.value_of("port").unwrap_or("35423").parse() {
            Ok(port) => port,
            Err(err) => {
                eprintln!("{}", err);
                process::exit(1);
            }
        };
        if let Err(err) = subcommand_repl(address, port) {
            eprintln!("{}", err);
            process::exit(1);
        }
    }

    if let Some(matches) = matches.subcommand_matches("file") {
        let file = matches.value_of("file").unwrap();
        let address = matches.value_of("address").unwrap_or("127.0.0.1");
        let port = match matches.value_of("port").unwrap_or("35423").parse() {
            Ok(port) => port,
            Err(err) => {
                eprintln!("{}", err);
                process::exit(1);
            }
        };
        if let Err(err) = subcommand_file(file, address, port) {
            eprintln!("{}", err);
            process::exit(1);
        }
    }

    if let Some(matches) = matches.subcommand_matches("keyboard") {
        let address = matches.value_of("address").unwrap_or("127.0.0.1");
        let port = match matches.value_of("port").unwrap_or("35423").parse() {
            Ok(port) => port,
            Err(err) => {
                eprintln!("{}", err);
                process::exit(1);
            }
        };
        if let Err(err) = subcommand_interactive(address, port) {
            eprintln!("{}", err);
            process::exit(1);
        }
    }
}
